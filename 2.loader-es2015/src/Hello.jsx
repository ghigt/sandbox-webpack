import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Hello extends Component {
  render() {
    const hello = Object.assign({hello: 'world'});
    return (
      <h1>Hello {hello.hello}</h1>
    );
  }
}

ReactDOM.render(<Hello />, document.getElementById('hello'));
